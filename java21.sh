#!/bin/bash
# Documentation from https://docs.papermc.io/misc/java-install - if this breaks, visit there for updates.

sudo apt install ca-certificates apt-transport-https gnupg wget
wget -O - https://apt.corretto.aws/corretto.key | sudo gpg --dearmor -o /usr/share/keyrings/corretto-keyring.gpg && \
echo "deb [signed-by=/usr/share/keyrings/corretto-keyring.gpg] https://apt.corretto.aws stable main" | sudo tee /etc/apt/sources.list.d/corretto.list
sudo apt update
sudo apt install -y java-21-amazon-corretto-jdk libxi6 libxtst6 libxrender1
echo "Done! Verify with java --version"
